# molecule-ci

## 概要
MoleculeとはAnsibleで作成したRoleをテストするためのツールです。
本リポジトリではローカル環境でMolecule実行環境を使用したAnsible Playbookのテストと、
GitlabCIを用いたAnsible Playbookのテストの自動実行のサンプルを提供します。

## ローカル環境でのMolecule実行環境セットアップ
### 対象環境
- MacOS

### 前提
- Python3がインストールされていること

### 実行環境セットアップ手順
1. moleculeインストール
  - `pip3 install molecule`
2. docker-pyインストール
  - `pip3 install docker`

### molecule実行手順
#### 1. lint
- リポジトリのあるディレクトリまで移動する。
- `molecule lint`

#### 2. unit test
- リポジトリのあるディレクトリまで移動する。
- 環境構築: `molecule converge`
- unit test実行: `molecule verify`
- 環境削除: `molecule destroy`

## Gitlab環境でのmolecule実行手順
1. `git push`
詳細は`.gitlab-ci.yml`を確認ください。

## 参照
- molecule公式ドキュメント: https://molecule.readthedocs.io/en/latest/